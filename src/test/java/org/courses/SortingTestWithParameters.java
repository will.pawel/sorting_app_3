package org.courses;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertTrue;

/**
 * Parameterized cases tests class
 */
@RunWith(Parameterized.class)
public class SortingTestWithParameters {
    /**
     * New sorting object for testing
     */
    protected Sorting sorting_param = new Sorting();
    private int[] given;
    private int[] expected;

    /**
     * Test constructor
     * @param given The given array of integers
     * @param expected The expected array of integers
     */
    public SortingTestWithParameters(int[] given, int[] expected) {
        this.given = given;
        this.expected = expected;
    }

    /**
     * Test method
     */
    @Test
    public void testDifferentCases() {
        sorting_param.sort(given);
        assertTrue(Arrays.equals(expected, given));
    }

    /**
     * Collection of parameters
     * @return Requested for tests parameters
     */
    @Parameterized.Parameters
    public static Collection<Object[]> given() {
        return Arrays.asList(new Object[][]{
                {new int[]{1, 2, 3, 4, 5}, new int[]{1, 2, 3, 4, 5}},
                {new int[]{2, 5, 3, 4, 5, 5, 3, 1, 12, 14}, new int[]{1, 2, 3, 3, 4, 5, 5, 5, 12, 14}},
                {new int[]{8, 1, 3, 2, 6, 5}, new int[]{1, 2, 3, 5, 6, 8}},
                {new int[]{5, 126, 22, 18, 5}, new int[]{5, 5, 18, 22, 126}},
        });
    }


}
