package org.courses;

import java.util.Arrays;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

/**
 * Corner cases tests class
 */
public class SortingTestWithoutParameters {
    Sorting sorting = new Sorting();

    /**
     * Null array test
     */
    @Test(expected = IllegalArgumentException.class)
    public void testNullCase(){
        sorting.sort(null);
    }

    /**
     * Empty array test
     */
    @Test
    public void testEmptyCase(){
        int[] given = {};
        int[] expected = {};
        sorting.sort(given);
        assertTrue(Arrays.equals(expected, given));
    }

    /**
     * Array longer than 10 integers test
     */
    @Test(expected = IllegalArgumentException.class)
    public void testLongerThanTen(){
        int[] given = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        sorting.sort(given);
    }

    /**
     * Single element test
     */
    @Test
    public void testSingleElementArrayCase() {
        int[] given = {1};
        int[] expected = {1};
        sorting.sort(given);
        assertTrue(Arrays.equals(expected, given));
    }

    /**
     * Ten integers test
     */
    @Test
    public void testTenNumbersCase() {
        int[] given = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        int[] expected = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        sorting.sort(given);
        assertTrue(Arrays.equals(expected, given));
    }


}
