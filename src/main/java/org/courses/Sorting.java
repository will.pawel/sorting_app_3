package org.courses;

import java.util.Arrays;
import java.util.StringJoiner;

/**
 * Main Sorting class
 */
public class Sorting {
    /**
     * Main method
     * @param args The string of entered values
     */
    public static void main(String[] args) {
        try {
            int[] numbers = convert(args);
            sort(numbers);
            StringJoiner stringJoiner = new StringJoiner(", ");
            Arrays.stream(numbers)
                    .mapToObj(String::valueOf)
                    .forEach(stringJoiner::add);
            System.out.println(stringJoiner);
        } catch (NumberFormatException e) {
            System.out.println("Entered values are not integers");
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Method which converts string of args into integer class
     * @param args String of entered values
     * @return Array of integers
     */
    public static int[] convert(String[] args){
        int[] numbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            numbers[i] = Integer.parseInt(args[i]);
        }

        return numbers;
    }

    /**
     * Method which sorts array of integers
     * @param array Sorted array of integers
     */
    public static void sort(int[] array) {

        if (array == null || array.length == 0) {
            throw new IllegalArgumentException("No numbers were entered");
        }

        if (array.length > 10){
            throw new IllegalArgumentException("Array is longer than 10 numbers");
        }

        Arrays.sort(array);

    }
}
